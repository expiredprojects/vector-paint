package rainhead.com;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 22.04.13
 * Time: 13:49
 * To change this template use File | Settings | File Templates.
 */
public class MyWindow extends JFrame{
    public MyWindow(){
        super("Vector PAINT");

        Container container = this.getContentPane();

        DrawPanel drawPanel = new DrawPanel();
        container.add(drawPanel, BorderLayout.CENTER);

        MyPanel myPanel = new MyPanel(drawPanel);
        container.add(myPanel, BorderLayout.PAGE_END);

        this.setVisible(true);
    }
}