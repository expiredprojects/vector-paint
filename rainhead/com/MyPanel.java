package rainhead.com;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 22.04.13
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
public class MyPanel extends JPanel implements ActionListener{
    public static final int myWidth = 700;
    public static final int myHeight = 50;

    private static final Color myBackground = new Color(100, 100, 100);
    private static final Color myForeground = new Color(240, 240, 240);

    private final DrawPanel drawPanel;
    private JRadioButton[] radioButtons;
    private ButtonGroup buttonGroup;
    private JTextField[] colorFields;
    private JButton[] saveButtons;

    private int r = 0, g = 0, b = 0;

    public MyPanel(DrawPanel _drawPanel){
        super();
        setBackground(myBackground);
        setPreferredSize(new Dimension(myWidth, myHeight));
        this.setLayout(new GridBagLayout());

        drawPanel = _drawPanel;
        initialize();
    }

    private void initialize() {
        radioButtons = new JRadioButton[3];
        buttonGroup = new ButtonGroup();
        colorFields = new JTextField[3];
        saveButtons = new JButton[3];

        radioButtons[0] = new JRadioButton("Line", true);
        radioButtons[1] = new JRadioButton("Rectangle");
        radioButtons[2] = new JRadioButton("Circle");

        for (int i = 0; i < colorFields.length; i ++){
            colorFields[i] = new JTextField("0", 3);
        }

        saveButtons[0] = new JButton("Save");
        saveButtons[1] = new JButton("Load");
        saveButtons[2] = new JButton("Export");

        skinElements();
        addElements();
    }

    private void skinElements() {
        for (int i = 0; i < radioButtons.length; i++){
            radioButtons[i].setBackground(myBackground);
            radioButtons[i].setForeground(myForeground);
            buttonGroup.add(radioButtons[i]);
        }
        for (int i = 0; i < colorFields.length; i++){
            colorFields[i].setBackground(myBackground);
            colorFields[i].setForeground(myForeground);
        }
        for (int i = 0; i < saveButtons.length; i++){
            saveButtons[i].setBackground(myBackground);
            saveButtons[i].setForeground(myForeground);
        }
    }

    private void addElements() {
        for (int i = 0; i < radioButtons.length; i++){
            radioButtons[i].addActionListener(this);
            this.add(radioButtons[i], new GridBagConstraints());
        }
        this.add(new JLabel("            "), new GridBagConstraints());
        for (int i = 0; i < colorFields.length; i++){
            colorFields[i].addActionListener(this);
            this.add(colorFields[i], new GridBagConstraints());
            this.add(new JLabel("   "), new GridBagConstraints());
        }
        this.add(new JLabel("            "), new GridBagConstraints());
        for (int i = 0; i < saveButtons.length; i++){
            saveButtons[i].addActionListener(this);
            this.add(saveButtons[i], new GridBagConstraints());
            this.add(new JLabel("   "), new GridBagConstraints());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source.equals(radioButtons[0])){
            drawPanel.drawMode = DrawPanel.DRAW_LINE;
        }
        else if (source.equals(radioButtons[1])){
            drawPanel.drawMode = DrawPanel.DRAW_RECT;
        }
        else if (source.equals(radioButtons[2])){
            drawPanel.drawMode = DrawPanel.DRAW_CIRCLE;
        }
        else if (source.equals(colorFields[0])){
            checkLimits(colorFields[0]);
            r = Integer.parseInt(colorFields[0].getText());
            drawPanel.recolor(r, g, b);
        }
        else if (source.equals(colorFields[1])){
            checkLimits(colorFields[1]);
            g = Integer.parseInt(colorFields[1].getText());
            drawPanel.recolor(r, g, b);
        }
        else if (source.equals(colorFields[2])){
            checkLimits(colorFields[2]);
            b = Integer.parseInt(colorFields[2].getText());
            drawPanel.recolor(r, g, b);
        }
        else if (source.equals(saveButtons[0])){
            drawPanel.saveState();
        }
        else if (source.equals(saveButtons[1])){
            drawPanel.loadState();
        }
        else if (source.equals(saveButtons[2])){
            drawPanel.exportImage();
        }
    }

    private void checkLimits(JTextField colorField) {
        if (Integer.parseInt(colorField.getText()) > 255){
            colorField.setText("255");
        }
        else if (Integer.parseInt(colorField.getText()) < 0){
            colorField.setText("0");
        }
    }
}