package rainhead.com;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 22.04.13
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class DrawPanel extends JPanel implements MouseInputListener{
    private static final String FILE_NAME = "save_state.txt";
    private static final Color myBackground = new Color(200, 200, 200);

    public static final int DRAW_LINE = 0;
    public static final int DRAW_RECT = 1;
    public static final int DRAW_CIRCLE = 2;
    public int drawMode = DRAW_LINE;

    private static final int DRAW_MODE = 0;
    private static final int MODIFY_MODE = 1;
    private static final int TRANSLATE_MODE = 2;
    private static final int DELETE_MODE = 3;
    private int currentMode;

    private final int TOUCH_RANGE = 20;
    private Color drawColor = new Color(0, 0, 0);

    private List<Color> colorList;
    private List<Shape> shapeList;
    private Shape ghostShape = null;

    private Point startPoint, endPoint;
    private Graphics2D graphics2D;
    private boolean MODIFY_END;

    public DrawPanel(){
        super();
        setBackground(myBackground);
        colorList = new ArrayList<Color>();
        shapeList = new ArrayList<Shape>();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void hideComment(){
        /*
        Implement the simple vector graphics editor.
        The editor makes possible to create vector images consisting of line segments, rectangles and circles.
        The required functionality of the editor comprises:
        − insertion of new elements to the image (lines, rectangles, circles),
        − modification of position of the selected image element,
        − deletion of an image element,
        − color setting for the selected element,
        − saving of the vector image in a text file,
        − loading the previously stored vector image so that all vector elements previously saved can be manipulated,
        − saving the image area as a raster image.
        Apply the following scenarios to particular actions:

        Adding of the new line segment:
        − click the point that is out of sensitivity area of other already created elements with
        mouse left button - the new line start point is at the point of click,
        − drag the mouse to the point, where the second line end should be positioned - the line
        connecting the click point with the current cursors position should be displayed
        instantly (use XOR line drawing mode to avoid the complete pane redisplay),
        − release the button at the position of the second line end.

        Adding of the circle:
        − click at the position of the circle center with the left mouse button,
        − drag the mouse to the position on the circle edge (in this way determine the circle
        radius) - the temporal circle should be redrawn after each mouse movement,
        − release the mouse button at the required distance from the circle center.

        Adding of a rectangle:
        − click at the position of the leftmost rectangle vertex,
        − drag to the position of the opposite rectangle vertex - the temporal rectangle should be
        redrawn after each mouse movement,
        − release the mouse.

        Which shape is to be added using one of operations described above depends on the shape
        type selection determined by three radio buttons located in bottom part the window (use individual button for each shape type).

        Modification of the line segment end position:
        − click with the left button near the end of the line segment - the close line segment end
        should be selected for modification and attracted to the cursor,
        − drag the mouse to the new desired position of the line end - the line in its temporal
        position should be redrawn after each mouse movement,
        − release the mouse button.

        Translation of the whole shape:
        − click near the center of the shape (line, rectangle, circle) with the left mouse button,
        − drag the shape center to the new position - the shape in its temporal position should be
        redrawn after each mouse movement,
        − release the button at the desired shape center position.

        Deletion of the shape:
        − click near to the shape center with the right mouse button

        The vector image store and load operation as well as the operation of the raster image writing to the graphic file
        should be invoked by appropriate push buttons.
        Use three edit fields for RGB values of the current color.
        The new shape should be created in the current color set by RGB values in these fields.
        */
    }

    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        graphics2D = (Graphics2D) graphics;
        drawShapes();
    }

    private void drawShapes() {
        for (int i = 0; i < shapeList.size(); i++){
            graphics2D.setColor(colorList.get(i));
            graphics2D.draw(shapeList.get(i));
        }

        if (ghostShape != null && currentMode == DRAW_MODE){
            graphics2D.setColor(drawColor);
            graphics2D.draw(ghostShape);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        startPoint = e.getPoint();
        currentMode = DRAW_MODE;
        checkMode(e);
    }

    private void checkMode(MouseEvent e) {
        Rectangle2D ghostBounds;
        for (int i = 0; i < shapeList.size(); i++){
            ghostBounds = shapeList.get(i).getBounds2D();
            if (e.getButton() == 3 &&
                    Math.abs(startPoint.getX() - ghostBounds.getCenterX()) < TOUCH_RANGE &&
                    Math.abs(startPoint.getY() - ghostBounds.getCenterY()) < TOUCH_RANGE){
                currentMode = DELETE_MODE;
                colorList.remove(i);
                shapeList.remove(i);
            }
            else if (e.getButton() == 1 &&
                    Math.abs(startPoint.getX() - ghostBounds.getCenterX()) < TOUCH_RANGE &&
                    Math.abs(startPoint.getY() - ghostBounds.getCenterY()) < TOUCH_RANGE){
                currentMode = TRANSLATE_MODE;
                ghostShape = shapeList.get(i);
            }
            else if (shapeList.get(i) instanceof Line2D.Double && (
                    (Math.abs(startPoint.getX() - ((Line2D.Double) shapeList.get(i)).getX2()) < TOUCH_RANGE &&
                            Math.abs(startPoint.getY() - ((Line2D.Double) shapeList.get(i)).getY2()) < TOUCH_RANGE))){
                MODIFY_END = true;
                currentMode = MODIFY_MODE;
                ghostShape = shapeList.get(i);
            }
            else if (shapeList.get(i) instanceof Line2D.Double &&
                    (Math.abs(startPoint.getX() - ((Line2D.Double) shapeList.get(i)).getX1()) < TOUCH_RANGE &&
                            Math.abs(startPoint.getY() - ((Line2D.Double) shapeList.get(i)).getY1()) < TOUCH_RANGE)){
                MODIFY_END = false;
                currentMode = MODIFY_MODE;
                ghostShape = shapeList.get(i);
            }
        }
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        endPoint = e.getPoint();
        if (currentMode == DRAW_MODE){
            constructShape();
            if (ghostShape != null){
                colorList.add(drawColor);
                shapeList.add(ghostShape);
            }
        }
        ghostShape = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        endPoint = e.getPoint();
        if (currentMode == DRAW_MODE){
            constructShape();
        }
        else if (currentMode == MODIFY_MODE){
            modifyShape();
        }
        else if (currentMode == TRANSLATE_MODE){
            translateShape();
        }
    }

    private void constructShape() {
        if (Math.abs(startPoint.getX() - endPoint.getX()) < TOUCH_RANGE && Math.abs(startPoint.getY() - endPoint.getY()) < TOUCH_RANGE){
            return;
        }

        switch (drawMode){
            case DRAW_LINE:{
                ghostShape = new Line2D.Double(startPoint, endPoint);
            }
            break;
            case DRAW_RECT:{
                if (startPoint.getX() < endPoint.getX() && startPoint.getY() < endPoint.getY()){
                    ghostShape = new Rectangle2D.Double(startPoint.getX(), startPoint.getY(),
                            endPoint.getX() - startPoint.getX(),  endPoint.getY() - startPoint.getY());
                }
                else if (startPoint.getX() < endPoint.getX()){
                    ghostShape = new Rectangle2D.Double(startPoint.getX(), endPoint.getY(),
                            endPoint.getX() - startPoint.getX(),  startPoint.getY() - endPoint.getY());
                }
                else if (startPoint.getY() < endPoint.getY()){
                    ghostShape = new Rectangle2D.Double(endPoint.getX(), startPoint.getY(),
                            startPoint.getX() - endPoint.getX(),  endPoint.getY() - startPoint.getY());
                }
                else{
                    ghostShape = new Rectangle2D.Double(endPoint.getX(), endPoint.getY(),
                            startPoint.getX() - endPoint.getX(),  startPoint.getY() - endPoint.getY());
                }
            }
            break;
            case DRAW_CIRCLE:{
                double radiusX = Math.pow(startPoint.getX() - endPoint.getX(), 2);
                double radiusY = Math.pow(startPoint.getY() - endPoint.getY(), 2);
                double radius = Math.sqrt(radiusX + radiusY);
                ghostShape = new Ellipse2D.Double(startPoint.getX() - radius, startPoint.getY() - radius,
                        2 * radius, 2 * radius);
            }
            break;
        }
        repaint();
    }

    private void modifyShape() {
        if (ghostShape instanceof Line2D.Double){
            if (MODIFY_END){
                ((Line2D.Double) ghostShape).x2 = endPoint.getX();
                ((Line2D.Double) ghostShape).y2 = endPoint.getY();
            }
            else {
                ((Line2D.Double) ghostShape).x1 = endPoint.getX();
                ((Line2D.Double) ghostShape).y1 = endPoint.getY();
            }
        }
        repaint();
    }

    private void translateShape() {
        if (ghostShape instanceof Line2D.Double){
            double ghostWidth = (((Line2D.Double) ghostShape).getX1() + ((Line2D.Double) ghostShape).getX2()) / 2 - ((Line2D.Double) ghostShape).getX1();
            double ghostHeight = (((Line2D.Double) ghostShape).getY1() + ((Line2D.Double) ghostShape).getY2()) / 2 - ((Line2D.Double) ghostShape).getY1();
            ((Line2D.Double) ghostShape).x1 = endPoint.getX() - ghostWidth;
            ((Line2D.Double) ghostShape).y1 = endPoint.getY() - ghostHeight;
            ((Line2D.Double) ghostShape).x2 = endPoint.getX() + ghostWidth;
            ((Line2D.Double) ghostShape).y2 = endPoint.getY() + ghostHeight;
        }
        else if (ghostShape instanceof Rectangle2D.Double){
            ((Rectangle2D.Double) ghostShape).x = endPoint.getX() - ((Rectangle2D.Double) ghostShape).getWidth() / 2;
            ((Rectangle2D.Double) ghostShape).y = endPoint.getY() - ((Rectangle2D.Double) ghostShape).getHeight() / 2;
        }
        else if (ghostShape instanceof Ellipse2D.Double){
            ((Ellipse2D.Double) ghostShape).x = endPoint.getX() - ((Ellipse2D.Double) ghostShape).getWidth() / 2;
            ((Ellipse2D.Double) ghostShape).y = endPoint.getY() - ((Ellipse2D.Double) ghostShape).getHeight() / 2;
        }
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void recolor(int r, int g, int b){
        drawColor = new Color(r, g, b);
    }

    public void saveState(){
        try {
            FileWriter fileWriter = new FileWriter(FILE_NAME);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int i = 0; i < shapeList.size(); i++){
                bufferedWriter.write(Integer.toString(i));
                bufferedWriter.newLine();
                bufferedWriter.write(Integer.toString(colorList.get(i).getRGB()));
                bufferedWriter.newLine();
                bufferedWriter.write(shapeList.get(i).getClass().getCanonicalName());
                bufferedWriter.newLine();
                if (shapeList.get(i) instanceof Line2D.Double){
                    bufferedWriter.write(Integer.toString((int) (((Line2D.Double) shapeList.get(i)).getX1())));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) (((Line2D.Double) shapeList.get(i)).getY1())));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) (((Line2D.Double) shapeList.get(i)).getX2())));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) (((Line2D.Double) shapeList.get(i)).getY2())));
                    bufferedWriter.newLine();
                }
                else {
                    bufferedWriter.write(Integer.toString((int) shapeList.get(i).getBounds2D().getX()));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) shapeList.get(i).getBounds2D().getY()));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) shapeList.get(i).getBounds2D().getWidth()));
                    bufferedWriter.newLine();
                    bufferedWriter.write(Integer.toString((int) shapeList.get(i).getBounds2D().getHeight()));
                    bufferedWriter.newLine();
                }
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void loadState(){
        try {
            List<Color> colorSaveList = new ArrayList<Color>();
            List<Shape> shapeSaveList = new ArrayList<Shape>();
            String className;
            int x, y, w, h;

            String currentLine;
            FileReader fileReader = new FileReader(FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while (bufferedReader.readLine() != null){
                currentLine =  bufferedReader.readLine();
                colorSaveList.add(new Color(Integer.parseInt(currentLine)));
                currentLine =  bufferedReader.readLine();
                className = currentLine;
                currentLine =  bufferedReader.readLine();
                x = Integer.parseInt(currentLine);
                currentLine =  bufferedReader.readLine();
                y = Integer.parseInt(currentLine);
                currentLine =  bufferedReader.readLine();
                w = Integer.parseInt(currentLine);
                currentLine =  bufferedReader.readLine();
                h = Integer.parseInt(currentLine);
                if (className.contains("Line2D")){
                    shapeSaveList.add(new Line2D.Double(x, y, w, h));
                }
                else if (className.contains("Rectangle2D")){
                    shapeSaveList.add(new Rectangle2D.Double(x, y, w, h));
                }
                else if (className.contains("Ellipse2D")){
                    shapeSaveList.add(new Ellipse2D.Double(x, y, w, h));
                }
            }
            bufferedReader.close();
            colorList = colorSaveList;
            shapeList = shapeSaveList;
            repaint();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void exportImage(){
        BufferedImage bufferedImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(myBackground);
        graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
        drawShapes();
        try {
            ImageIO.write(bufferedImage, "PNG", new File("render.png"));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
