package rainhead;

import rainhead.com.MyPanel;
import rainhead.com.MyWindow;
import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        MyWindow myWindow = new MyWindow();
        myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myWindow.setVisible(true);

        setBounds(myWindow);
        setMinimumSize(myWindow);
    }

    private static void setBounds(MyWindow myWindow) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        myWindow.setBounds((int) (dimension.height * 0.05), (int) (dimension.height * 0.05),
                MyPanel.myWidth, (int) (dimension.height * 0.6));
    }

    private static void setMinimumSize(MyWindow myWindow) {
        Insets insets = myWindow.getInsets();
        myWindow.setMinimumSize(new Dimension(MyPanel.myWidth + insets.right + insets.left,
                MyPanel.myHeight + insets.top + insets.bottom));
    }
}